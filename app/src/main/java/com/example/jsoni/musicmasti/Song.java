package com.example.jsoni.musicmasti;

public class Song {
    private String songAlbumTitle;
    private String songTitle;
    private String songSingers;
    private int    songAlbumCoverId;

    public Song(String albumTitle, String title, String singers, int albumCoverId) {

        songAlbumTitle = albumTitle;
        songTitle = title;
        songSingers = singers;
        songAlbumCoverId = albumCoverId;

    }

    public String getSongAlbumTitle() {
        return songAlbumTitle;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public String getSongSingers() {
        return songSingers;
    }

    public int getSongAlbumCoverId() {
        return songAlbumCoverId;
    }
}
