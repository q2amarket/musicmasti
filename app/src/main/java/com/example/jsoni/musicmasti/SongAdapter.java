package com.example.jsoni.musicmasti;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class SongAdapter extends ArrayAdapter<Song> {

    private int     playingIndex = -1;
    private boolean currentTrack = false;
    private boolean isAllPlayClicked = false;

    public SongAdapter(@NonNull Context context, int resource, @NonNull List<Song> objects) {
        super(context, resource, objects);
    }

    public SongAdapter(@NonNull Context context, int resource, @NonNull List<Song> objects, boolean isAllPlay) {
        super(context, resource, objects);
        isAllPlayClicked = isAllPlay;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View             playlistItemView = convertView;
        final ViewHolder holder;

        if (playlistItemView == null) {
            playlistItemView = LayoutInflater.from(getContext()).inflate(R.layout.playlist_item, parent, false);

            holder = new ViewHolder();

            holder.albumCoverThumbnail = playlistItemView.findViewById(R.id.playlist_album_thumbnail);
            holder.songTitle = playlistItemView.findViewById(R.id.playlist_song_title);
            holder.songAlbumTitle = playlistItemView.findViewById(R.id.playlist_song_album_title);
            holder.songArtist = playlistItemView.findViewById(R.id.playlist_song_artist);
            holder.songPlayButton = playlistItemView.findViewById(R.id.playlist_play_button);

            playlistItemView.setTag(holder);
        } else {
            holder = (ViewHolder) playlistItemView.getTag();
        }

        // get the current song index
        final Song currentSong = getItem(position);

        // set data to the list item
        assert currentSong != null;
        holder.albumCoverThumbnail.setImageResource(currentSong.getSongAlbumCoverId());
        holder.songTitle.setText(currentSong.getSongTitle());
        holder.songAlbumTitle.setText(currentSong.getSongAlbumTitle());
        holder.songArtist.setText(currentSong.getSongSingers());

        // play first track on Play All button clicked
        if (isAllPlayClicked) {
            playingIndex = 0;
            holder.songPlayButton.setImageResource(R.drawable.ic_pause_black_24dp);
            isAllPlayClicked = false;
            currentTrack = !currentTrack;
        }

        // check the play status of the song item
        if (playingIndex == position) {
            holder.songPlayButton.setImageResource(R.drawable.ic_pause_black_24dp);
        } else {
            holder.songPlayButton.setImageResource(R.drawable.ic_play_arrow_black_24dp);
        }

        // set song button action
        holder.songPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.songPlayButton.setImageResource(R.drawable.ic_pause_black_24dp);

                // pause current song on other click
                if (position == playingIndex) {
                    playingIndex = -1;
                } else {
                    playingIndex = position;
                    notifyDataSetChanged();
                }

                // pause play current track
                if (currentTrack) {
                    holder.songPlayButton.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                    currentTrack = !currentTrack;
                } else {
                    holder.songPlayButton.setImageResource(R.drawable.ic_pause_black_24dp);
                }
                currentTrack = !currentTrack;
            }
        });

        return playlistItemView;
    }

    // get the fields
    static class ViewHolder {
        ImageView   albumCoverThumbnail;
        TextView    songTitle;
        TextView    songAlbumTitle;
        TextView    songArtist;
        ImageButton songPlayButton;
    }
}