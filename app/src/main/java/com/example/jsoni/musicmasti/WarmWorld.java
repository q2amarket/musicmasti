package com.example.jsoni.musicmasti;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class WarmWorld extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        // get view ids
        ImageView albumCoverImage = findViewById(R.id.album_cover);

        // get intent extras
        Bundle bundle = getIntent().getExtras();
        // check if bundle in not null and containing value
        if (bundle != null) {

            String albumTitle = bundle.getString("album_three_title");
            String albumBand = bundle.getString("album_three_band");
            int albumCover = bundle.getInt("album_three_cover");

            albumCoverImage.setImageResource(albumCover);

            TextView albumTitleText = findViewById(R.id.album_title);
            TextView albumBandText = findViewById(R.id.album_band);

            albumTitleText.setText(albumTitle);
            albumBandText.setText(albumBand);

            final ArrayList<Song> songs = new ArrayList<Song>();

            // get the thumbnail resource id
            int albumThumb = R.drawable.album_three_thumbnail;

            songs.add(new Song(albumTitle, "You have warm my heart", "Mona Mika", albumThumb));
            songs.add(new Song(albumTitle, "Wonderful life wonderful heart", "Jacker Donald", albumThumb));
            songs.add(new Song(albumTitle, "You have warm my heart", "Mona Mika", albumThumb));
            songs.add(new Song(albumTitle, "Wonderful life wonderful heart", "Jacker Donald", albumThumb));
            songs.add(new Song(albumTitle, "You have warm my heart", "Mona Mika", albumThumb));
            songs.add(new Song(albumTitle, "Wonderful life wonderful heart", "Jacker Donald", albumThumb));
            songs.add(new Song(albumTitle, "You have warm my heart", "Mona Mika", albumThumb));
            songs.add(new Song(albumTitle, "Wonderful life wonderful heart", "Jacker Donald", albumThumb));
            songs.add(new Song(albumTitle, "You have warm my heart", "Mona Mika", albumThumb));
            songs.add(new Song(albumTitle, "Wonderful life wonderful heart", "Jacker Donald", albumThumb));
            songs.add(new Song(albumTitle, "You have warm my heart", "Mona Mika", albumThumb));
            songs.add(new Song(albumTitle, "Wonderful life wonderful heart", "Jacker Donald", albumThumb));

            SongAdapter songAdapter = new SongAdapter(this, 0, songs);

            final ListView listView = findViewById(R.id.playlist_view);
            listView.setAdapter(songAdapter);

            // get the Play All button
            Button playAll = findViewById(R.id.album_play_all_button);

            // Play All button click event
            playAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SongAdapter newAdapter = new SongAdapter(WarmWorld.this, 0, songs, true);
                    listView.setAdapter(newAdapter);
                }
            });

        } // @TODO move this brace after albumBand.setText(bundle.getString("album_three_band"));
    }
}
