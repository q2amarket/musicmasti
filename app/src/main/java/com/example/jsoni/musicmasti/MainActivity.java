package com.example.jsoni.musicmasti;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // album one button intent
        final ImageButton albumOne = findViewById(R.id.album_one);

        // set on click listener
        albumOne.setOnClickListener(new View.OnClickListener() {
            // override the require method onClick
            @Override
            public void onClick(View view) {
                // create intent to open the DreamVoyage activity
                Intent albumOneIntent = new Intent(MainActivity.this, DreamVoyage.class);
                // add extra values
                albumOneIntent.putExtra("album_one_cover", R.drawable.album_one);
                albumOneIntent.putExtra("album_one_title", getString(R.string.dream_voyage));
                albumOneIntent.putExtra("album_one_band", getString(R.string.gibson_band));
                // start the new activity
                startActivity(albumOneIntent);
            }
        });

        // album two button intent
        final ImageButton albumTwo = findViewById(R.id.album_two);

        // set on click listener
        albumTwo.setOnClickListener(new View.OnClickListener() {
            // override the require method onClick
            @Override
            public void onClick(View view) {
                // create intent to open the DreamVoyage activity
                Intent albumTwoIntent = new Intent(MainActivity.this, HiddenTruth.class);
                // add extra values
                albumTwoIntent.putExtra("album_two_cover", R.drawable.album_two);
                albumTwoIntent.putExtra("album_two_title", getString(R.string.hidden_truth));
                albumTwoIntent.putExtra("album_two_band", getString(R.string.bamboo_buzz));
                // start the new activity
                startActivity(albumTwoIntent);
            }
        });

        // album three button intent
        final ImageButton albumThree = findViewById(R.id.album_three);

        // set on click listener
        albumThree.setOnClickListener(new View.OnClickListener() {
            // override the require method onClick
            @Override
            public void onClick(View view) {
                // create intent to open the DreamVoyage activity
                Intent albumThreeIntent = new Intent(MainActivity.this, WarmWorld.class);
                // add extra values
                albumThreeIntent.putExtra("album_three_cover", R.drawable.album_three);
                albumThreeIntent.putExtra("album_three_title", getString(R.string.warm_world));
                albumThreeIntent.putExtra("album_three_band", getString(R.string.vision_world));
                // start the new activity
                startActivity(albumThreeIntent);
            }
        });

        // album four button intent
        final ImageButton albumFour = findViewById(R.id.album_four);

        // set on click listener
        albumFour.setOnClickListener(new View.OnClickListener() {
            // override the require method onClick
            @Override
            public void onClick(View view) {
                // create intent to open the DreamVoyage activity
                Intent albumFourIntent = new Intent(MainActivity.this, LiveShow.class);
                // add extra values
                albumFourIntent.putExtra("album_four_cover", R.drawable.album_four);
                albumFourIntent.putExtra("album_four_title", getString(R.string.live_show));
                albumFourIntent.putExtra("album_four_band", getString(R.string.roaking_rose));
                // start the new activity
                startActivity(albumFourIntent);
            }
        });
    }

    private void createShortcut(){
        Intent shortcutIntent = new Intent("android.permission.INSTALL_SHORTCUT");
    }
}
